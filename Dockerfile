FROM ubuntu:kinetic-20230624@sha256:e322f4808315c387868a9135beeb11435b5b83130a8599fd7d0014452c34f489
# make the 'app' folder the current working directory

WORKDIR /app

# copy project files and folders to the current working directory (i.e. 'app' folder)
COPY solana-accountsdb-connector/target/release/solana-geyser-connector-raw .

RUN chmod +x solana-geyser-connector-raw
CMD [ "/app/solana-geyser-connector-raw", "/config.toml" ]
