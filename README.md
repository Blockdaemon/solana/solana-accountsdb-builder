The Rust version must be the same as used to compile for solana.
This can be figured out by going to the solana repo: https://github.com/solana-labs/solana/actions/workflows/client-targets.yml

Click the first job, and look for a linux build, for example:
https://github.com/solana-labs/solana/runs/8239118131?check_suite_focus=true

Expand "Run actions-rs/toolchain@v1"

Look for something like this:

`  stable-x86_64-unknown-linux-gnu unchanged - rustc 1.63.0 (4b91a6ea7 2022-08-08)`


This means version 1.63.0 of rust needs to be used during building of the plugin.


== Artifacts / Docker image ==

The gitlab ci procudes 2 artifacts and a docker image.

Artifacts:

- geyser-plugin-grpc
  The Solana Geyser plugin. It opens a gRPC server (see proto/) and broadcasts account and slot updates to all clients that connect.
- connector-raw
  A connector binary built on lib/ that stores raw binary account data in PostgreSQL.
  1st parameter is the config file

Docker image:

- connector-raw
  A containerized version of the connector raw.
  See example config.
